from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse
#from basico.views import teste


class Profile_extras(models.Model):
    user = models.OneToOneField(User)
    data = models.DateField()
    def __unicode__(self):
        return self.user.username

class Post(models.Model):
    #user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    titulo = models.CharField(max_length=120)
    imagem = models.ImageField(upload_to='teste', null=True, blank=True)
    conteudo = models.TextField()

    def get_absolute_url(self):
        return reverse("detalhe", kwargs={"pk": self.pk})

    def get_absolute_urls(self):
        return reverse("author_delete", kwargs={"pk": self.pk})

    def get_absolute_urlss(self):
        return reverse("update", kwargs={"pk": self.pk})

    def __unicode__(self):
        return self.titulo


