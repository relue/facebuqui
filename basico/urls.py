from django.conf.urls import patterns, url
from basico import views
from basico.views import *

urlpatterns = patterns('',
        url(r'^login/', views.logina, name='logina'),
        url(r'^registrar/', views.register, name='registrar'),
        url(r'^sobre/', Sobre.as_view(), name='sobre'),
        url(r'^logout/', views.user_logout, name='logoutt'),
        url(r'^criate/', Criar.as_view(), name='criar'),
        url(r'^home/', Listar.as_view(), name='listar'),
        url(r'view/(?P<pk>\d+)/$', Detalhe.as_view(), name='detalhe'),
        url(r'update/(?P<pk>\d+)/$', Update.as_view(), name='update'),
    	url(r'delete/(?P<pk>\d+)$', Deletar.as_view(), name='author_delete'),
    	url (r'^usuarios/',ListarUsuarios.as_view(), name='usuarios'),
    	url(r'^users/(?P<pk>\w+)/$', AbrirUsuarios.as_view(), name='listarusuarios'),


        #url(r'^perfil/(?P<usuario>\w+)/$', views.usuario, name='usuario'),
        #url(r'^perfil/', views.post, name='post'),
        #$url(r'^list/', views.post_list, name='list'),
)