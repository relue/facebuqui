from django import forms
from basico.models import Profile_extras, Post
#from inicio.models import Post
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
	username = forms.CharField(help_text="Entre com seu usuario.")
	password = forms.CharField(widget=forms.PasswordInput(),help_text="Entre com sua senha.")
	email = forms.EmailField(help_text="Entre com seu email.")
	class Meta:
		model = User
		fields = ['username', 'password', 'email']

class UserProfileForm(forms.ModelForm):
	data = forms.DateField(help_text='Entre com sua data de nascimento.')
	class Meta:
		model = Profile_extras
		fields = ['data']

#class PostForm(forms.ModelForm):
 #   class Meta:
  #      model = Post
   #     fields = [
    #        "title",
     #       "content",
      #      "image",
       # ]