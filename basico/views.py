from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from basico.forms import UserForm, UserProfileForm
from basico.models import Post, Profile_extras
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse_lazy




def logina(request):
    context = RequestContext(request)
    if request.method == 'POST':
        usuario = request.POST['username']
        senha = request.POST['password']
        user = authenticate(username=usuario, password=senha)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/basico/home/' + usuario)
            else:
                return HttpResponse("Sua conta esta desabilitada!")
        else:
            print "Invalid login details: {0}, {1}".format(user, senha)
            return HttpResponse("Login invalido detalhes supplied.")
    else:
        return render_to_response('visual/login.html', {}, context)


def register(request):
    context = RequestContext(request)
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            
            profile = profile_form.save(commit=False)
            #profile.set_password(user.senha)
            profile.user = user
            profile.save()
            registered = True

        else:
            print user_form.errors, profile_form.errors
    else:
        user_form    = UserForm()
        profile_form = UserProfileForm()
    #print user_form

    return render_to_response('visual/register.html',{'user_form': user_form, 'profile_form': profile_form, 'registered': registered},context)

class Sobre(TemplateView):

    template_name = "visual/sobre.html"

    def dispatch(self, *args, **kwargs):
        return super(Sobre, self).dispatch(*args, **kwargs)

@login_required
def user_logout(request):
    logout(request)
    # Take the user back to the homepage.
    return HttpResponseRedirect('/basico/login')

class Criar(CreateView):
    model = Post
    fields = ['titulo', 'conteudo', 'imagem']
    template_name  = 'visual/criar.html'
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Criar, self).dispatch(*args, **kwargs)
    def form_valid(self, form):
        form_teste = form.save(commit=False)
        form_teste.user = self.request.user
        form_teste.save()
        return super(Criar, self).form_valid(form)
 


class Listar(ListView):

    model = Post
    template_name = 'visual/listar.html'


    def get_queryset(self, **kwargs):
        return Post.objects.filter(user = self.request.user)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Listar, self).dispatch(*args, **kwargs)


class Detalhe(DetailView):
    slug_field = 'pk'
    model = Post
    context_object_name = 'meu_artigo'
    template_name = 'visual/detalhe.html'


    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Detalhe, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return self.model.objects.filter()

class Update(UpdateView):
    model = Post
    fields = ['titulo', 'conteudo', 'imagem']
    template_name  = 'visual/criar.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Update, self).dispatch(*args, **kwargs)


class Deletar(DeleteView):
    model = Post
    success_url = reverse_lazy('listar')
    template_name = 'visual/deletarr.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Deletar, self).dispatch(*args, **kwargs)

class ListarUsuarios(ListView):
    model = Profile_extras
    template_name = 'visual/usuarios.html'

    def get_queryset(self, **kwargs):
        return Profile_extras.objects.filter()

class AbrirUsuarios(ListView):
    model = Post
    template_name = 'visual/usuarios_teste.html'
    #x = Post()
    #x.user = user = User.objects.get(id=1)
    #print x.user
    def get_queryset(self, **kwargs):
        pk=self.kwargs['pk']
        print pk
        return Post.objects.filter(user = User.objects.get(id=pk) )